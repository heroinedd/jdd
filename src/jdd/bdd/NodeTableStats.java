package jdd.bdd;

import jdd.util.JDDConsole;

public class NodeTableStats {
    int table_size;
    int free_nodes_count;
    int stat_nt_grow;
    long stat_grow_time;
    int dead_nodes;
    int root_nodes;
    long ht_chain;
    int stat_lookup_count;
    int stat_gc_count;
    long stat_gc_freed;
    long stat_gc_time;
    long stat_notify_time;

    public NodeTableStats(int table_size, int free_nodes_count, int stat_nt_grow, long stat_grow_time, int dead_nodes, int root_nodes, long ht_chain, int stat_lookup_count, int stat_gc_count, long stat_gc_freed, long stat_gc_time, long stat_notify_time) {
        this.table_size = table_size;
        this.free_nodes_count = free_nodes_count;
        this.stat_nt_grow = stat_nt_grow;
        this.stat_grow_time = stat_grow_time;
        this.dead_nodes = dead_nodes;
        this.root_nodes = root_nodes;
        this.ht_chain = ht_chain;
        this.stat_lookup_count = stat_lookup_count;
        this.stat_gc_count = stat_gc_count;
        this.stat_gc_freed = stat_gc_freed;
        this.stat_gc_time = stat_gc_time;
        this.stat_notify_time = stat_notify_time;
    }

    public int get_table_size() {
        return this.table_size;
    }

    public int get_free_nodes_count() {
        return this.free_nodes_count;
    }

    public int get_stat_nt_grow() {
        return this.stat_nt_grow;
    }

    public long get_stat_grow_time() {
        return this.stat_grow_time;
    }

    public int get_dead_nodes() {
        return this.dead_nodes;
    }

    public int get_root_nodes() {
        return this.root_nodes;
    }

    public long get_ht_chain() {
        return this.ht_chain;
    }

    public int get_stat_lookup_count() {
        return this.stat_lookup_count;
    }

    public int get_stat_gc_count() {
        return this.stat_gc_count;
    }

    public long get_stat_gc_freed() {
        return this.stat_gc_freed;
    }

    public long get_stat_gc_time() {
        return this.stat_gc_time;
    }

    public long get_stat_notify_time() {
        return this.stat_notify_time;
    }

    public void showStats() {
        JDDConsole.out.printf("NT nodes=%d free=%d #grow=%d grow-time=%d dead=%d root=%d\n", new Object[]{this.table_size, this.free_nodes_count, this.stat_nt_grow, this.stat_grow_time, this.dead_nodes, this.root_nodes});
        JDDConsole.out.printf("HT chain=%d access=%d\n", new Object[]{this.ht_chain, this.stat_lookup_count});
        JDDConsole.out.printf("GC count=%d #freed=%d time=%d signal-time=%d\n", new Object[]{this.stat_gc_count, this.stat_gc_freed, this.stat_gc_time, this.stat_notify_time});
    }
}